/***
 *    ███████╗██╗██████╗ ███████╗██████╗ ██╗  ██╗ ██████╗ ███████╗███╗   ██╗██╗██╗  ██╗
 *    ██╔════╝██║██╔══██╗██╔════╝██╔══██╗██║  ██║██╔═══██╗██╔════╝████╗  ██║██║╚██╗██╔╝
 *    █████╗  ██║██████╔╝█████╗  ██████╔╝███████║██║   ██║█████╗  ██╔██╗ ██║██║ ╚███╔╝ 
 *    ██╔══╝  ██║██╔══██╗██╔══╝  ██╔═══╝ ██╔══██║██║   ██║██╔══╝  ██║╚██╗██║██║ ██╔██╗ 
 *    ██║     ██║██║  ██║███████╗██║     ██║  ██║╚██████╔╝███████╗██║ ╚████║██║██╔╝ ██╗
 *    ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝
 *                      Firebase implementation for Phoenix                                                                     
 */

// Dependencies
const functions = require('firebase-functions'); // Cloud Functions SDK

// Express layer
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({ origin: true })); // Automatically allow cross-origin requests

// Universal HTTPS API:
app.get('**', async (req, res) => {
    return res.send(req.body);
});
exports.httpsAPI = functions.https.onRequest(app);

// Firebase SDK compatible callable API:
exports.callableAPI = functions.https.onCall((data, context) => {
    let jsonPayload = data;
    return jsonPayload;
});

// Realtime Database trigger powered API:
exports.realtimeAPI = functions.database.ref('/messageAPI/{userUid}')
    .onWrite((change, context) => {
        if (!change.after.exists()) {
            return null;  // No processing is necessary after a remove command or a null writing
        }
        // The payload written to the endpoint:
        const jsonPayload = change.after.val();
        return jsonPayload;
    });

