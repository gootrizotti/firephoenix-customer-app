// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";
import "firebase/functions";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyCYTpcVZfD41N-7wsst9O3-i5NTLRopZiI",
    authDomain: "kolina-phoenix.firebaseapp.com",
    databaseURL: "https://kolina-phoenix.firebaseio.com",
    projectId: "kolina-phoenix",
    storageBucket: "kolina-phoenix.appspot.com",
    messagingSenderId: "113049304274",
    appId: "1:113049304274:web:78f673619b24654fa6810b"
};

// prevent initialize Firebase twice
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
    if (location.hostname === 'localhost' && process.browser) {
        firebase.functions().useFunctionsEmulator("http://localhost:5001");
    }
}

var fbModule = {
    getInstance: function() {
        return firebase;
    }
}

export default async function (context, inject) {
    inject("firebase", firebase)
}